﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageViewer : MonoBehaviour
{
    public MeasureDepth _mesureDepth;
    public MultiSourceManager _mulitSource;
    public RawImage _rawImage;
    public RawImage _rawDepth;
    void Update()
    {
        _rawImage.texture = _mulitSource.GetColorTexture();

        _rawDepth.texture = _mesureDepth._depthTexture;
    }
}
