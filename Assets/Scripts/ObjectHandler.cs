﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class ObjectHandler : MonoBehaviour
{
    private List<stopAnimation> _lStopAnimations = new List<stopAnimation>();
    private List<PlayingAnimation> _lPlayingAnimation = new List<PlayingAnimation>();

    //functions
    public void Awake()
    {
        /*
        int windowHeight = Screen.height;
        int windowWidth = Screen.width;

        if (!Screen.fullScreen)
            Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true, 0);
        else
            Screen.SetResolution(windowWidth, windowHeight, false, 0);
        */
    }
    public void Update()
    {
        tryStopAnimations();
        tryPlayingAnimation();
    }
    public void rotateImgToCam(GameObject image, FixedAxeis fixedAxis = FixedAxeis.Y)
    {
        GameObject obj = Camera.main.gameObject;
        var lookPos = image.transform.position - obj.transform.position;

        switch (fixedAxis)
        {
            default:
                break;
            case FixedAxeis.X:
                lookPos.x = 0;
                break;
            case FixedAxeis.Y:
                lookPos.y = 0;
                break;
            case FixedAxeis.Z:
                lookPos.z = 0;
                break;
        }
        image.transform.rotation = Quaternion.LookRotation(lookPos);
        return;
    }

    #region Animation
    private void tryPlayingAnimation()
    {
        
        if (_lPlayingAnimation.Count <= 0)
            return;
        for (int i = _lPlayingAnimation.Count - 1; i >= 0; i--)
        {
            if (_lPlayingAnimation[i].testStoping())
            {
                _lPlayingAnimation.RemoveAt(i);
            }
        }
        
    }
    private void tryStopAnimations()
    {
        if (_lStopAnimations.Count <= 0)
            return;
        for (int i = _lStopAnimations.Count - 1; i >= 0; i--)
        {
            if (_lStopAnimations[i].stop())
                _lStopAnimations.RemoveAt(i);
        }
    }
    public bool playAnimation(Animation animation, AnimationClip clip, float offset, OffsetType offsetType)
    {
        continueAnimation(animation);
        if (animation.GetClip(clip.name) == null)
            animation.AddClip(clip, clip.name);
        if (animation.Play(clip.name))
        {
            switch (offsetType)
            {
                default:
                case OffsetType.Time:
                    animation[clip.name].time = offset;
                    break;
                case OffsetType.Frame:
                    animation[clip.name].normalizedTime = offset / (clip.frameRate * clip.length);
                    break;
                case OffsetType.Relative:
                    animation[clip.name].normalizedTime = offset;
                    break;
            }
            _lPlayingAnimation.Add(new PlayingAnimation(this, animation, clip.name));
            return true;

        }
        return false;
    }
    public void playAnimationQueue(Animation animation, List<AnimationClip> clipList)
    {
        continueAnimation(animation);
        if (clipList == null || clipList.Count < 1)
            return;
        bool playFirst = true;
        for (int i = 0; i < clipList.Count; i++)
        {
            if (clipList[i] == null)
                continue;
            if (playFirst)
            {
                if (!animation.GetClip(clipList[i].name))
                    animation.AddClip(clipList[i], clipList[i].name);
                animation.Play(clipList[i].name);
                playFirst = false;
            }
            else
            {
                if (!animation.GetClip(clipList[i].name))
                    animation.AddClip(clipList[i], clipList[i].name);
                animation.PlayQueued(clipList[i].name);
            }
        }
    }
    public void pauseAnimation(Animation animation)
    {
        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                state.speed = 0;
                return;
            }
        }
    }
    public bool continueAnimation(Animation animation)
    {
        foreach (AnimationState state in animation)
            state.speed = 1;
        return true;
    }
    public void stopAnimation(Animation animation, bool resetTime = true)
    {
        continueAnimation(animation);

        if (resetTime)
        {
            foreach (AnimationState state in animation)
            {
                if (animation.IsPlaying(state.name))
                {
                    state.time = 0f;
                    state.speed = 0f;
                }
            }
            _lStopAnimations.Add(new stopAnimation(animation, 2));
        }
        else
            _lStopAnimations.Add(new stopAnimation(animation, 0));
    }
    public bool setAnimationTime(Animation animation, float offset, OffsetType type)
    {
        if (!animation.isPlaying)
            return false;

        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                switch (type)
                {
                    default:
                    case OffsetType.Time:
                        state.time = offset;
                        break;
                    case OffsetType.Relative:
                        state.normalizedTime = (offset / (state.clip.frameRate * state.length));
                        break;
                    case OffsetType.Frame:
                        state.normalizedTime = offset;
                        break;
                }
                return true;
            }
        }
        return false;
    }
    public float getAnimationTime(Animation animation, OffsetType type)
    {
        if (!animation.isPlaying)
            return 0;

        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                float time = state.time;
                while (time > state.length)
                    time -= state.length;
                switch(type)
                {
                    case OffsetType.Time:
                        return time;
                    case OffsetType.Frame:
                        return time * state.clip.frameRate;
                    case OffsetType.Relative:
                        return time/state.length;
                }
            }
        }
        return 0;
    }
    public float getAnimationLegth(Animation animation)
    {
        if (!animation.isPlaying)
            return 0;
        //AnimationState state = new AnimationState();
        //foreach (ref AnimationState state in animation) Error CS1510
        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                return state.length;
            }
        }
        return 0;
    }
    public string getAnimationName(Animation animation)
    {
        if (!animation.isPlaying)
            return "";
        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                return state.name;
            }
        }
        return "";
    }
    #endregion

    #region colorBlock
    public void changeColorBlock(GameObject obj, Color color, ColorType type)
    {
        Component[] com = obj.GetComponents(typeof(Component));

        for (int i = 0; i < com.Length; i++)
        {
            var v = com[i].GetType().GetProperty("colors");
            if (v == null)
                continue;
            ColorBlock colors = (ColorBlock)v.GetValue(com[i]);

            switch (type)
            {
                case ColorType.NormalColor:
                    colors.normalColor = color;
                    break;
                case ColorType.HighlightedColor:
                    colors.highlightedColor = color;
                    break;
                case ColorType.PressedColor:
                    colors.pressedColor = color;
                    break;
                case ColorType.SelectedColor:
                    colors.selectedColor = color;
                    break;
                case ColorType.DisabledColor:
                    colors.disabledColor = color;
                    break;
            }
            v.SetValue(com[i], colors);
        }
    }

    #endregion

    #region touch
    public float getTouchPinch(int fingerCount)
    {
        if (fingerCount > 1 && Input.touchCount >= fingerCount)
        {
            Vector2 touch0 = new Vector2();
            for (int i = 0; i < fingerCount; i++)
            {

                touch0 += Input.GetTouch(0).deltaPosition;
            }
            return (touch0 / fingerCount).magnitude;
        }
        else
            return 0f;
    }
    public float getTouchPinch()
    {
        if (Input.touchCount < 2)
            return 0;

        Vector2 midpoint = new Vector2();
        float distanceNow = 0;
        float distanceThen = 0;
        for (int i = 0; i < Input.touchCount; i++)
        {
            midpoint += Input.GetTouch(i).position;
        }
        midpoint = midpoint / Input.touchCount;

        for (int i = 0; i < Input.touchCount; i++)
        {
            distanceNow += (Input.GetTouch(i).position - midpoint).magnitude;
            distanceThen += ((Input.GetTouch(i).position - Input.GetTouch(i).deltaPosition) - midpoint).magnitude;
        }

        float t = (float)Math.Sqrt(Screen.currentResolution.height * Screen.currentResolution.height + Screen.currentResolution.width * Screen.currentResolution.width);

        return (distanceNow - distanceThen) / t;
    }
    #endregion
}



public enum ColorType
{
    NormalColor,
    HighlightedColor,
    PressedColor,
    SelectedColor,
    DisabledColor
}
struct stopAnimation
{
    public Animation animation;
    public int waitFrame;
    public stopAnimation(Animation anim, int wait)
    {
        foreach (AnimationState state in anim)
        {
        }

        animation = anim;
        waitFrame = wait;
    }
    public bool stop()
    {
        if (waitFrame-- <= 0)
        {
            animation.Stop();
            return true;
        }
        return false;
    }
}
struct PlayingAnimation
{
    public ObjectHandler _oh;
    public Animation animation;
    public string name;
    public float time;
    public PlayingAnimation(ObjectHandler oh, Animation animation, string name)
    {
        _oh = oh;
        this.animation = animation;
        this.name = name;
        switch (animation[name].wrapMode)
        {
            case WrapMode.Once:
            default:
                time = animation[name].length;
                break;
            case WrapMode.PingPong:
                time = 2 * animation[name].length;
                break;
        }

    }
    public bool testStoping()
    {
        if (animation.IsPlaying(name) && (animation[name].time >= time))
        {
            _oh.stopAnimation(animation);
            return true;
        }
        return false;
    }
}
public enum OffsetType
{
    Time,
    Relative,
    Frame
}
public enum FixedAxeis
{
    None,
    X,
    Y,
    Z
}
