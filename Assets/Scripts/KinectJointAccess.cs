﻿using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;
using Kinect = Windows.Kinect;

public class KinectJointAccess : MonoBehaviour
{
    private BodySourceManager _bodySourceManager;
    private MultiSourceManager _multiSourceManager;
    //public GameObject BodySourceManager;

    private ulong _trackedBodyId = 0;
    private Body _trackedBody = null;
    private Camera _cameraMain = null;

    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    void Start()
    {
        _cameraMain = Camera.main;
        TryGetComponent(out _bodySourceManager);
        TryGetComponent(out _multiSourceManager);
    }
    void Update()
    {
        if (_bodySourceManager == null && !TryGetComponent(out _bodySourceManager))
        {
            return;
        }
        Kinect.Body[] data = _bodySourceManager.GetData();
        if (data == null)
        {
            _trackedBodyId = 0;
            _trackedBody = null;
            return;
        }

        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
                continue;

            if (body.IsTracked)
                trackedIds.Add(body.TrackingId);
        }
        if (!trackedIds.Contains(_trackedBodyId))
        {
            _trackedBodyId = 0;
            _trackedBody = null;
            foreach (var body in data)
            {
                if (body == null)
                {
                    continue;
                }
                if (body.IsTracked)
                {
                    _trackedBodyId = body.TrackingId;
                    _trackedBody = body;
                    break;
                }
            }
        }
    }
    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
    public Vector3 getJointScreenPosition(Kinect.JointType joint)
    {
        if (_cameraMain == null || _trackedBody == null)
            return Vector3.zero;

        Kinect.Joint sourceJoint = _trackedBody.Joints[joint];
        Vector2 posRelative =  _multiSourceManager.mapCameraPointToRelativeColorSpace(sourceJoint.Position);
        Vector3 pos = new Vector3(posRelative.x * _cameraMain.pixelWidth, posRelative.y * _cameraMain.pixelHeight, sourceJoint.Position.Z);

        Debug.Log(pos.ToString());

        if (float.IsInfinity(pos.x) || float.IsInfinity(pos.y) || float.IsInfinity(sourceJoint.Position.Z))
            return Vector3.zero;
        return pos;
    }
    public Vector3 getJointPosition(Kinect.JointType joint)
    {
        if (_trackedBody == null)
            return Vector3.zero;
        Kinect.Joint sourceJoint = _trackedBody.Joints[joint];
        Vector3 targetPosition = GetVector3FromJoint(sourceJoint);

        //_trackedBody.JointOrientations[joint].Orientation;

        return targetPosition;
    }
    public double getJointAngle(Kinect.JointType joint1, Kinect.JointType joint2, Kinect.JointType joint3)
    {
        if (_trackedBody == null)
            return 0;

        Kinect.Joint j1 = _trackedBody.Joints[joint1];
        Kinect.Joint j2 = _trackedBody.Joints[joint2];
        Kinect.Joint j3 = _trackedBody.Joints[joint3];

        Vector3 a = new Vector3(0, j1.Position.Y - j2.Position.Y, j1.Position.Z - j2.Position.Z);
        Vector3 b = new Vector3(0, j3.Position.Y - j2.Position.Y, j3.Position.Z - j2.Position.Z);
        a.Normalize();
        b.Normalize();
        return Vector3.Angle(a, b);
    }
}
