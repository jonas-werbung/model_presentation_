﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CameraHandler : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject _cameraModel;
    [SerializeField] public AnimationCurve _curveCameraProgress;

    Transform _mainCam;
    Camera _mainCamera;
    public Transform _rotPoint;
    float _fRotPointDistance;
    public float _moveToActiveSpeed;

    private Transform _transformTo;

    // cam transition
    Vector3     _v3PositionStart = new Vector3();
    Quaternion  _quRotationStart = new Quaternion();
    Vector3     _v3LookAtStart = new Vector3();
    Transform   _lookAtEnd = null;
    Vector3     _v3UpStart = new Vector3();
    Vector3     _v3ForwardStart = new Vector3();
    float       _distanceStart = 0;
    float       _distanceEnd = 0;
    float       _fieldOfViewStart = 0;
    float       _fieldOfViewTarget = 0;
    float       _nearClipPlaneStart = 1;
    float       _nearClipPlaneTarget = 1;
    float       _farClipPlaneStart = 1000;
    float       _farClipPlaneFarTarget = 1000;

    float _transisionTime = 1.0f;

    public float _zoomSpeed;
    public float _rotSpeed;
    public float _moveSpeed;
    
    float _transisionTimeProgress = 0;
    float _minCamDist = 0.5f;

    //private bool _bTransitionOverflow = false;
    bool _bFirstUbdate = true;
    bool _bfreeCam = true;
    bool _bCamTransision = false;
    bool _bCamTransisionStrait = false;
    bool _bInteractable = false;

    void Awake()
    {
        _mainCam = Camera.main.transform;
        _mainCam.TryGetComponent(out _mainCamera);
    }
    void Start()
    {
        deactivateAllCams();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!_bCamTransision && !_bfreeCam)
            updateCamModleMovment();
    }
    void Update()
    {
        if (_bCamTransision)
            moveCamToActive();
    }
    private void deactivateAllCams()
    {
        for (int i = Camera.allCamerasCount - 1; i >= 0; i--)
        {
            if (Camera.main != Camera.allCameras[i])
                Camera.allCameras[i].gameObject.SetActive(false);
            else
                Camera.allCameras[i].gameObject.SetActive(true);
        }
        Camera.main.gameObject.SetActive(true);
    }
    void updateCamModleMovment()
    {
        if (_bFirstUbdate)
        {
            _bFirstUbdate = false;
            if(_distanceEnd == 0)
                _fRotPointDistance = (_mainCam.position - _rotPoint.position).magnitude;
            else
                _rotPoint.position = _mainCam.position + _mainCam.forward * _distanceEnd;
        }

        if (Camera.main == null || _cameraModel == null)
            return;

        _mainCam.position = _cameraModel.transform.position;

        if (_lookAtEnd == null)
            _mainCam.rotation = _cameraModel.transform.rotation;
        else
        {
            _mainCam.LookAt(_lookAtEnd.position, Vector3.up);
        }
        _mainCam.localScale = _cameraModel.transform.lossyScale;
    }
    void setUpFreeCam()
    {
        //todo: rotPoint nearest to Center of GameObject?
        if(_lookAtEnd == null)
            _rotPoint.position = _mainCam.position + _mainCam.forward * _fRotPointDistance;

        _bfreeCam = true;
        _bCamTransision = false;
    }

    #region Camera Transition
    public void changeCamera(GameObject cam, float time = 1, bool interactable = false, bool straitTransition = true)
    {
        changeCamera(cam, time, interactable, straitTransition, null);
    }
    public void changeCamera(GameObject cam, float time = 1, bool interactable = false, bool straitTransition = true, Transform lookAt = null)
    {
        //check for Camera
        Camera camGoal;
        if (cam == null || !cam.TryGetComponent<Camera>(out camGoal))
            return;
        if (!_bfreeCam && _cameraModel == cam)
            return;

        //set new Camera
        _bfreeCam = false;
        _bInteractable = interactable;
        _cameraModel = cam;

        //check for hard cut
        if (time <= 0 || (_mainCam.position == cam.transform.position && _mainCam.rotation == _cameraModel.transform.rotation))
        {
            if (lookAt != null)
            {
                _lookAtEnd = lookAt;
                _rotPoint.position = _lookAtEnd.position;
            }
            else
            {
                _lookAtEnd = null;
            }
            return;
        }

        //prepare transition
        _bCamTransisionStrait = straitTransition;
        _transisionTime = time;
        _bCamTransision = true;

        _v3PositionStart = _mainCam.position;
        _quRotationStart = _mainCam.rotation;

        _transformTo = _cameraModel.transform;

        _nearClipPlaneStart = _mainCamera.nearClipPlane;
        _farClipPlaneStart = _mainCamera.farClipPlane;
        _nearClipPlaneTarget = camGoal.nearClipPlane;
        _farClipPlaneFarTarget = camGoal.farClipPlane;

        // lookAts
        Vector3 lookAtLast;
        CalculateLineLineIntersection(_mainCam.position, _mainCam.position + _mainCam.forward, _cameraModel.transform.position,
            _cameraModel.transform.position + _cameraModel.transform.forward, out _v3LookAtStart, out lookAtLast);

        if (lookAt != null)
        {
            _lookAtEnd = lookAt;
            lookAtLast = lookAt.position;
        }
        else
        {
            _rotPoint.position = lookAtLast;
            _lookAtEnd = null;
        }

        _v3ForwardStart = _mainCam.forward;
        _v3UpStart = _mainCam.up;

        _distanceStart = (_mainCam.position - _v3LookAtStart).magnitude;
        _distanceEnd = (_cameraModel.transform.position - lookAtLast).magnitude;

        _transisionTimeProgress = 0;

        _fRotPointDistance = _distanceEnd;
        //fieldOfView
        Camera camera;
            _fieldOfViewStart = _mainCamera.fieldOfView;
        if (_cameraModel.TryGetComponent(out camera))
        {
            _fieldOfViewTarget = camera.fieldOfView;
        }
        else
            _fieldOfViewTarget = 0;
        Debug.DrawLine(_cameraModel.transform.position, _mainCam.transform.position, Color.red, 20);

        Debug.DrawRay(_cameraModel.transform.position, _cameraModel.transform.forward * 300, Color.green, 20);
        Debug.DrawRay(_mainCam.transform.position, _mainCam.transform.forward * 300, Color.cyan, 20);

        Debug.DrawLine(_v3LookAtStart, lookAtLast, Color.blue, 20);
    }
    private void moveCamToActive()
    {
        _transisionTimeProgress = _transisionTimeProgress + (Time.deltaTime / _transisionTime);

        //calculate curveProgress
        float curveProgress = _curveCameraProgress.Evaluate(_transisionTimeProgress / _transisionTime);

        if (curveProgress >= 1)
        {
            _bCamTransision = false;
            curveProgress = 1.0f;
        }

        Vector3 lookAt, lookUp;
        if (_lookAtEnd == null)
        {
            Vector3 lookAt_1, lookAt_2;
            CalculateLineLineIntersection(_v3LookAtStart - _v3PositionStart, _v3LookAtStart, _cameraModel.transform.position,
                _cameraModel.transform.position + _cameraModel.transform.forward, out lookAt_1, out lookAt_2);

            lookAt = Vector3.Lerp(lookAt_1, lookAt_2, curveProgress);
            _mainCam.transform.rotation = Quaternion.Lerp(_quRotationStart, _cameraModel.transform.rotation, curveProgress);
            _mainCam.position = Vector3.Lerp(_v3PositionStart, _cameraModel.transform.position, curveProgress);
        }
        else  //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        {
            _mainCam.position = Vector3.Lerp(_v3PositionStart, _cameraModel.transform.position, curveProgress);

            lookAt = Vector3.Lerp(_v3LookAtStart, _lookAtEnd.position, curveProgress);
            lookUp = Vector3.Lerp(_v3UpStart, Vector3.up, curveProgress);
            _mainCam.LookAt(lookAt, lookUp);
            _rotPoint.position = lookAt;

            Debug.DrawLine(_mainCam.transform.position + _v3UpStart, _cameraModel.transform.position + Vector3.up, Color.green, 10);
            Debug.DrawLine(_mainCam.transform.position, _mainCam.transform.position + lookUp, Color.yellow, 10);

            Debug.DrawLine(_v3LookAtStart, _lookAtEnd.position, Color.blue, 10);
            Debug.DrawLine(_mainCam.transform.position, lookAt, Color.cyan, 10);
        }

        if (!_bCamTransisionStrait)
        {
            float distance = Mathf.Lerp(_distanceStart, _distanceEnd, curveProgress);  //--------------------------------------------------------------------------------------------------------------
            _mainCam.position = lookAt - _mainCam.forward * distance;
        }
        //Debug.DrawLine(_mainCam.transform.position, lookAt, Color.black, 10);

        _mainCamera.fieldOfView = Mathf.Lerp(_fieldOfViewStart, _fieldOfViewTarget, curveProgress);
        _mainCamera.nearClipPlane = Mathf.Lerp(_nearClipPlaneStart, _nearClipPlaneTarget, curveProgress);
        _mainCamera.farClipPlane = Mathf.Lerp(_farClipPlaneStart, _farClipPlaneFarTarget, curveProgress);

    }
    private void camTransition(float curveProgress)
    {
        Vector3 lookAt, lookUp;
        if (_lookAtEnd == null)
        {
            lookAt = Vector3.Lerp(_v3LookAtStart, _cameraModel.transform.position + _cameraModel.transform.forward, curveProgress);
            _mainCam.transform.rotation = Quaternion.Lerp(_quRotationStart, _cameraModel.transform.rotation, curveProgress);
        }
        else
        {
            lookAt = Vector3.Lerp(_v3LookAtStart, _lookAtEnd.position, curveProgress);
            lookUp = Vector3.Lerp(_v3UpStart, Vector3.up, curveProgress);
            _mainCam.transform.LookAt(lookAt, lookUp);
        }

        _mainCam.position = Vector3.Lerp(_v3PositionStart, _cameraModel.transform.position, curveProgress);

        if (!_bCamTransisionStrait)
        {
            float distance;
            if(_lookAtEnd == null)
                distance = Mathf.Lerp(_distanceStart, 1, curveProgress);
            else
                distance = Mathf.Lerp(_distanceStart, _distanceEnd, curveProgress);
            _mainCam.position = lookAt - _mainCam.forward * distance;
        }
    }
    private void camTransitionStrait(float curveProgress)
    {
        Vector3 lookAt, lookUp;
        if (_lookAtEnd != null)
        {
            lookAt = Vector3.Lerp(_v3PositionStart + _v3ForwardStart, _lookAtEnd.position, curveProgress);
            lookUp = Vector3.Lerp(_v3UpStart, Vector3.up, curveProgress);
        }
        else
        {
            lookUp = Vector3.Lerp(_v3UpStart, _cameraModel.transform.up, curveProgress);
            lookAt = Vector3.Lerp(_v3PositionStart + _v3ForwardStart, _cameraModel.transform.position + _cameraModel.transform.forward, curveProgress);
        }

        _mainCam.position = Vector3.Lerp(_v3PositionStart, _cameraModel.transform.position, curveProgress);
        _mainCam.transform.LookAt(lookAt, lookUp);

        if (_bCamTransisionStrait)
        {
            float distance = Mathf.Lerp(_distanceStart, _distanceEnd, curveProgress);
            _mainCam.position = lookAt - _mainCam.forward * distance;
        }
    }
    private void camTransitionRound(float curveProgress)
    {
        //rotation
        if (_lookAtEnd == null)
        {
            _mainCam.rotation = Quaternion.Lerp(_quRotationStart, _cameraModel.transform.rotation, curveProgress);
        }
        else
        {
            _mainCam.rotation = Quaternion.Lerp(_quRotationStart, Quaternion.Euler(_cameraModel.transform.position - _lookAtEnd.position), curveProgress);
        }

        //location
        Vector3 lookAt1, lookAt2;
        if(_lookAtEnd == null)
            CalculateLineLineIntersection(_v3PositionStart, _v3PositionStart + _v3ForwardStart,
            _cameraModel.transform.position, _cameraModel.transform.position + _cameraModel.transform.forward,
            out lookAt1, out lookAt2);
        else
        {
            lookAt1 = _v3PositionStart + _v3ForwardStart * _distanceStart;
            lookAt2 = _lookAtEnd.position;
        }

        Vector3 lookAt = Vector3.Lerp(lookAt1, lookAt2, curveProgress);


        _rotPoint.position = lookAt;

        float distance = Mathf.Lerp(_distanceStart, _distanceEnd, curveProgress);
        _mainCam.position = lookAt - _mainCam.forward * distance;
    }
    private static bool CalculateLineLineIntersection(Vector3 line1Point1, Vector3 line1Point2,
    Vector3 line2Point1, Vector3 line2Point2, out Vector3 resultSegmentPoint1, out Vector3 resultSegmentPoint2)
    {
        // Algorithm is ported from the C algorithm of 
        // Paul Bourke at http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline3d/
        resultSegmentPoint1 = Vector3.zero;
        resultSegmentPoint2 = Vector3.zero;

        Vector3 p1 = line1Point1;
        Vector3 p2 = line1Point2;
        Vector3 p3 = line2Point1;
        Vector3 p4 = line2Point2;
        Vector3 p13 = p1 - p3;
        Vector3 p43 = p4 - p3;
        Vector3 p21 = p2 - p1;

        if (p43.sqrMagnitude < Mathf.Epsilon)
        {
            return false;
        }

        if (p21.sqrMagnitude < Mathf.Epsilon)
        {
            return false;
        }

        double d1343 = p13.x * (double)p43.x + (double)p13.y * p43.y + (double)p13.z * p43.z;
        double d4321 = p43.x * (double)p21.x + (double)p43.y * p21.y + (double)p43.z * p21.z;
        double d1321 = p13.x * (double)p21.x + (double)p13.y * p21.y + (double)p13.z * p21.z;
        double d4343 = p43.x * (double)p43.x + (double)p43.y * p43.y + (double)p43.z * p43.z;
        double d2121 = p21.x * (double)p21.x + (double)p21.y * p21.y + (double)p21.z * p21.z;

        double denom = d2121 * d4343 - d4321 * d4321;
        if (Math.Abs(denom) < Mathf.Epsilon)
        {
            return false;
        }
        double numer = d1343 * d4321 - d1321 * d4343;

        double mua = numer / denom;
        double mub = (d1343 + d4321 * (mua)) / d4343;

        resultSegmentPoint1.x = (float)(p1.x + mua * p21.x);
        resultSegmentPoint1.y = (float)(p1.y + mua * p21.y);
        resultSegmentPoint1.z = (float)(p1.z + mua * p21.z);

        resultSegmentPoint2.x = (float)(p3.x + mub * p43.x);
        resultSegmentPoint2.y = (float)(p3.y + mub * p43.y);
        resultSegmentPoint2.z = (float)(p3.z + mub * p43.z);

        return true;
    }
    #endregion

    #region Free Camera Transformation
    public bool transformFreeCamera(float movementX, float movementY, float rotationX, float rotationY, bool selfrotation, float zoom)
    {
        return transformFreeCamera(new Vector2(movementX, movementY), new Vector2(rotationX, rotationY), selfrotation, zoom);
    }
    public bool transformFreeCamera(Vector2 movement, Vector2 rotation, bool selfrotation, float zoom)
    {
        bool b1 = moveFreeCamera(movement);
        bool b2 = rotateFreeCamera(rotation, selfrotation);
        bool b3 = zoomFreeCamera(zoom);

        return b1 && b2 && b3;
    }
    public bool moveFreeCamera(Vector2 movement)
    {
        if (!_bInteractable || (movement.x == 0 && movement.y == 0))
            return false;
        if (!_bfreeCam)
            setUpFreeCam();
        _bCamTransision = false;
        _bfreeCam = true;

        float camSacle = Vector3.Distance(_mainCam.position, _rotPoint.transform.position) * 0.18f;

        Vector3 translation = (_mainCam.right * movement.x * _moveSpeed * Time.deltaTime * (-1) * camSacle) +
            (_mainCam.up * movement.y * _moveSpeed * Time.deltaTime * (-1) * camSacle);

        _mainCam.Translate(translation, Space.World);

        return true;
    }
    public bool rotateFreeCamera(Vector2 rotation, bool self = false)
    {
        if (!_bInteractable || (rotation.x == 0 && rotation.y == 0))
            return false;
        if (!_bfreeCam)
            setUpFreeCam();

        rotation *= _rotSpeed * Time.deltaTime;

        Debug.Log("rotation: " + rotation.ToString() + "__self: " + self);

        if (self) // rotation Raound self
        {
            //rotat Cam
            _mainCam.RotateAround(_mainCam.position, Vector3.up, -rotation.x);
            _mainCam.RotateAround(_mainCam.position, _mainCam.right, rotation.y);

            //rotate lookAt
            _rotPoint.RotateAround(_mainCam.position, _mainCam.up, -rotation.x);
            _rotPoint.RotateAround(_mainCam.position, _mainCam.right, rotation.y);
        }
        else // rotation Raound lookAt
        {
            _mainCam.RotateAround(_rotPoint.position, Vector3.up, rotation.x);
            _mainCam.RotateAround(_rotPoint.position, _mainCam.right, -rotation.y);
        }

        _bfreeCam = true;
        _bCamTransision = false;
        _bfreeCam = true;

        return true;
    }
    public bool zoomFreeCamera(float value)
    {
        if (!_bInteractable || value == 0)
            return false;
        if (!_bfreeCam)
            setUpFreeCam();

        _bfreeCam = true;

        float dist = (_rotPoint.position - _mainCam.position).magnitude;
        float moveDist = value * _zoomSpeed * Time.deltaTime;

        if (moveDist > 0 && dist - moveDist < _minCamDist)
        {
            if (dist < _minCamDist)
            {
                _mainCam.Translate(Vector3.forward * (_minCamDist - dist), Space.Self);
            }
            return false;
        }
        _mainCam.Translate(Vector3.forward * moveDist, Space.Self);

        _bCamTransision = false;
        _bfreeCam = true;

        return true;
    }
    public bool setFreeCameraDistance(float value)
    {
        if (!_bInteractable)
            return false;
        if (!_bfreeCam)
            setUpFreeCam();

        _mainCam.position = _rotPoint.position - _mainCam.forward * value;

        return true;
    }
    public float getFreeCameraDistance()
    {
        return (_rotPoint.position - _mainCam.position).magnitude;
    }
    public float getDistanceToRotationPoint(GameObject gameObject)
    {
        return (_rotPoint.position - gameObject.transform.position).magnitude;
    }

    #endregion

    #region Focus Camera
    public void DebugDrawBounds(Vector3 v3Center, Vector3 v3Extents, float p_duration = 3)
    {
        Color color = Color.green;

        Vector3 v3FrontTopLeft;
        Vector3 v3FrontTopRight;
        Vector3 v3FrontBottomLeft;
        Vector3 v3FrontBottomRight;
        Vector3 v3BackTopLeft;
        Vector3 v3BackTopRight;
        Vector3 v3BackBottomLeft;
        Vector3 v3BackBottomRight;

        v3FrontTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top left corner
        v3FrontTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top right corner
        v3FrontBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom left corner
        v3FrontBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom right corner
        v3BackTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top left corner
        v3BackTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top right corner
        v3BackBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom left corner
        v3BackBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom right corner

        v3FrontTopLeft = transform.TransformPoint(v3FrontTopLeft);
        v3FrontTopRight = transform.TransformPoint(v3FrontTopRight);
        v3FrontBottomLeft = transform.TransformPoint(v3FrontBottomLeft);
        v3FrontBottomRight = transform.TransformPoint(v3FrontBottomRight);
        v3BackTopLeft = transform.TransformPoint(v3BackTopLeft);
        v3BackTopRight = transform.TransformPoint(v3BackTopRight);
        v3BackBottomLeft = transform.TransformPoint(v3BackBottomLeft);
        v3BackBottomRight = transform.TransformPoint(v3BackBottomRight);

    }
    public float calculateDistance(float p_FOV, float p_sizeOfObjekt)
    {
        return p_sizeOfObjekt / (2 * Mathf.Tan(p_FOV));
    }
    public void FocusCameraOnGameObject(Vector3 v3Center, Vector3 v3Extents)
    {
        /*
        if (!_bfreeCam)
            return;

        Camera cam = Camera.main;
        GameObject go = _oh.getActiveGameObjekt();

        //save positions/rotatioos vor transisions
        _v3CamTransitionFrom = cam.transform.position;
        _quCamTransitionFrom = cam.transform.rotation;
        _v3RotPointTransitionFrom = _rotPoint.position;

        DebugDrawBounds(v3Center, v3Extents);
        //get the smaller fov (horizontal, vertical)
        float horizontalFOV = 2f * Mathf.Atan(Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad / 2f) * cam.aspect) * Mathf.Rad2Deg;
        float fov = Math.Min(horizontalFOV, cam.fieldOfView);
        //calculate needed distance between Camera and bounds center
        float newDist = calculateDistance(fov, v3Extents.magnitude);
        
        cam.transform.Translate(v3Center - _v3RotPointTransitionFrom, Space.World);
        cam.transform.LookAt(v3Center);

        float oldDist = (v3Center - cam.transform.position).magnitude;
        cam.transform.Translate(cam.transform.forward * (oldDist-newDist), Space.World);

        //save new camera transvom for transition
        _v3CamTransitionTo = cam.transform.position;
        _quCamTransitionTo = cam.transform.rotation;
        _v3RotPointTransitionTo = v3Center;

        //reset camera transform for transition
        cam.transform.position = _v3CamTransitionFrom;
        cam.transform.rotation = _quCamTransitionFrom;
        //set booleans for transition
        _bCamTransision = true;
        _bRotPointTransition = true;
        */
        return;
    }
    #endregion

}

























